'use strict';

var React = require('react');

module.exports = React.createClass({
  
  getDefaultProps: function(){
    return {
      title: 'default title',
      childrend: '',
      appName: 'main'
    };
  },
  
  render: function(){
    return (
      <html>
        <head>
          <meta charSet='utf-8' />
          <title>
            {this.props.title}
          </title>
          <link rel="stylesheet" type="text/css" href={'/css/' + this.props.settings.appName + '.css'}/>
        </head>
        <body>
          {this.props.children}
        </body>
        <script src="/js/deps.js"></script>
        <script src={'/js/' + this.props.settings.appName + '.js'}></script>
      </html>
    );
  }
});