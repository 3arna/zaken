'use strict';

var paths     = {};
var root      = process.cwd();

paths.app     = root + '/_app/';
paths.shared  = root + '/_shared/';
paths.styles  = root + '/_shared/styles';
paths.public  = root + '/public/';
paths.css     = root + '/public/css/';
paths.js      = root + '/public/js/';

paths.sys     = root + '/_sys/';
paths.cfg     = root + '/_sys/cfg/';
paths.init    = root + '/_sys/init/';
paths.locales = root + '/_sys/locales/';

paths.lib     = root + '/lib/';
paths.utils   = root + '/lib/utils/';

paths.appTree = function(appName){
  return {
    views: root + '/_app/' + appName + '/views/',
    styles: root + '/_app/' + appName + '/styles/'
  }
};

module.exports = paths;
