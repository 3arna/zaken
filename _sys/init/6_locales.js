'use strict'

var i18n    = require('i18next'),
    paths   = require('../cfg').paths,
    cfg     = require('../cfg');

module.exports = function() {
  var env = this.settings.env;
  
  i18n.init({
      saveMissing: true,
      debug: false,
      fallbackLng: ['en', 'es'],
      resGetPath: paths.locales + '__lng__.json'
  });
  
  this.use(i18n.handle);
}