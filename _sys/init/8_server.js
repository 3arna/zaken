'use strict'

/*process.env.NODE_PATH = __dirname;
require('module').Module._initPaths();*/

var express = require('express'),
    paths   = require('../cfg').paths

module.exports = function(){
  var sys = this;
  
  this.use(express.static(paths.public));
  
  this.listen(process.env.PORT || 3000, process.env.IP, function(error){
    sys.settings.env == 'development'
      ? console.log ( error ? error : 'Express server listening '+ process.env.PORT || 3000)
      : false
  });
  
};