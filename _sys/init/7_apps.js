'use strict'

var fs    = require('fs'),
    paths = require('../cfg').paths

module.exports = function() {
  var sys = this;
  fs.readdirSync(paths.app).forEach(function(dirName){
    sys.use('/', require(paths.app + dirName));
  });
  
};