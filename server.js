/*jshint globalstrict: true*/
/*jshint node: true*/
'use strict';

var paths     = require('./_sys').cfg.paths,
    express   = require('express'),
    bootable  = require('bootable'),
    sys       = bootable(express());

sys.phase(bootable.initializers(paths.init));

sys.boot(function(err){
  if(err){
    process.exit(-1);
    throw err; 
  }
});