var fs = require('fs');
var paths = require('../_sys').cfg.paths;
var browserify = require('browserify');

var defaultJsx = 'app.jsx';
var dependenciesFile = 'deps.js';


var loadAll = process.argv.length === 2;

if(process.argv.indexOf('--dependencies') !== -1 || loadAll){
  var deps = browserify();
  deps.require('react').require('jquery').require('lodash');
  deps.bundle().pipe(fs.createWriteStream(paths.js + dependenciesFile));
  console.log('Jsx build', ' >> Proccessing << ', 'file browserification', dependenciesFile);
}


if(process.argv.indexOf('--views') !== -1 || loadAll){
  fs.readdirSync(paths.app).forEach(function(appName){
    var appTree = paths.appTree(appName);
    var filePath = appTree.views + defaultJsx;
    var exists = fs.existsSync(filePath);
    
    if(!exists){
      console.log('Jsx build', ' >> Warning << ', 'file does not exist', filePath);
      return false;
    }
    
    var b = browserify();
  
    b.add(filePath);
    b.transform('reactify');
    b.exclude('react').exclude('jquery').exclude('lodash');
    b.bundle().pipe(fs.createWriteStream(paths.js + appName + '.js'));
    console.log('Jsx build', ' >> Success << ', 'file browserified', paths.js + appName + '.js');
    
  });
}