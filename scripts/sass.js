var fs = require('fs');
var paths = require('../_sys').cfg.paths;
var sass = require('node-sass');

var defaultSass = 'style.scss';


var folders = fs.readdirSync(paths.app);
var finished = 0;
folders.forEach(function(appName){
  var appTree = paths.appTree(appName);
  var filePath = appTree.styles + defaultSass;
  var exists = fs.existsSync(filePath);
  
  if(!exists){
    console.log('Sass build', ' >> Warning << ', 'file does not exist', filePath);
    finished++;
    return false;
  }
  
  var file = fs.readFileSync(filePath, 'utf8').toString();
  
  var opts = {
    data: file,
    success: function(result) {
        fs.writeFile(paths.css + appName + '.css', result.css, function(e) {
          if(e) return console.error(e);
          console.log('Sass build', ' >> Success << ', 'file compiled', paths.css + appName + '.css');
          finished++;
          finished-1 === folders.length ? process.kill(0) : false;
        });
    },
    error: function(error) {
      console.log(error);
    },
    includePaths: [paths.styles, appTree.styles],
    outputStyle: 'compact'/*, 'compressed'*/
    
  }
  
  sass.render(opts);
  
});