'use strict';

var utils = require('./');

exports.appLoading = function(app){
  utils.str.debug(app.settings.env, '\n > Controllers & Views loaded [ ' + app.settings.appName + ' ]');
};

exports.appLoaded = function(app){
  utils.str.debug(app.settings.env, ' > Application [ ' + app.settings.appName + ' ]  loaded \n');
};

exports.appRoutes = function(app){
  var routes = app._router.stack;
  var gets = [' > GET routes'];
  var posts = [' > POST routes'];
  routes.forEach(function(route){
    if(!route.route) return false;
    route.route.methods.post ? posts.push(' >> ' + route.route.path) : false;
    route.route.methods.get ? gets.push(' >> ' + route.route.path) : false;
  });
  
  utils.str.debug(app.settings.env, posts.concat(gets));
};

exports.appNoPath = function(app){
  utils.str.debug(app.settings.env, 'App loading' + ' >> Warning << ' + 'cannot load app without options.path variable');
}