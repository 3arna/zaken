'use strict';

var fs = require('fs');
var React = require('react');
var lodash = require('lodash');
var utils = require('./');

exports.loadApp = function(app, options){
  
  if(app.settings.env === 'development'){
    if(!options.path) return utils.debug.appNoPath(app);
    app.settings.appPath = options.path;
    app.settings.appName = options.path.split('/').pop();
  }
  
  loadControllers(app, options);
  loadRoutes(app, options);
  loadViewEngine(app, options);
  
  utils.debug.appLoading(app);
  utils.debug.appRoutes(app);
  utils.debug.appLoaded(app);
  
};

var loadControllers = function(path, options){
  options.controllers = {};
  fs.readdirSync(options.path + '/controllers/').forEach(function(fileName){
    options.controllers[fileName.replace('.js', '')] = require(options.path + '/controllers/' + fileName);
  });
};

var loadRoutes = function(app, options){
  options.routes.forEach(function(route){
    app.get(route.url, lodash.get(options.controllers, route.ctr));
  });  
};

var loadViewEngine = function(app, appOptions){
  
  var engine = function(viewPath, options, callback){
    options.viewName = viewPath.split('/').pop().replace('.jsx', '');
    options.routes = appOptions.routes;
    options.route = lodash.find(options.routes, function(route){
      return route.url == options.route;
    });
    
    var html = render(options.settings.views + 'app.jsx', options);
    
    var clientData = [
      '<script id="viewData" type="text/javascript">',
        'var viewData = ' + JSON.stringify(options) + ';',
      '</script>'
    ].join('');
    
    html = html.replace('</body>', clientData + '</body>');
    return callback(null, html);
  };
    
  require('node-jsx').install();
  
  app.engine('jsx', engine);
  app.set('views', appOptions.path + '/views/');
  app.set('view engine', 'jsx');
  
}

var render = exports.renderView = function(viewPath, viewData){
  var view = require(viewPath);
  var factoryView = React.createFactory(view);
  var html = React.renderToString(factoryView(viewData));
  return html;
}