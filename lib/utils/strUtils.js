'use strict';

var fs = require('fs');

exports.firstUp = function(str){
  if(!str.length) return '';
  return str.charAt(0).toUpperCase() + str.substring(1);
};

exports.debug = function(env, args){
  if(env !== 'development') return false;
  if(typeof args === 'string'){
    return console.log(args);
  }
  args.forEach(function(arg){
    console.log(arg);
  });
  return false;
};