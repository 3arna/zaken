'use strict';

exports.default = function(req, res){
  //req.i18n.setLng('es');
  var viewData = {
    route: req.url
  }
  viewData.title = req.i18n.t('title');
  res.render('default', viewData);
};

exports.test = function(req, res){
  var viewData = {
    route: req.url,
  }
  res.render('test', viewData);
};

exports.testId = function(req, res){
  var viewData = {
    route: req.url,
  }
  res.render('test/id', viewData);
};

exports.about = function(req, res){
  var viewData = {
    route: req.url,
  }
  res.render('about', viewData);
};

exports.inbox = function(req, res){
  var viewData = {
    route: req.url,
  }
  res.render('inbox', viewData);
};