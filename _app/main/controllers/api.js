'use strict';

module.exports = function(app){
  
  var PRODUCTS = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'},
    {category: 'Electronics', price: '$97.99', stocked: false, name: 'Nexus 9'},
    {category: 'Electronics', price: '$52.99', stocked: false, name: 'Nexus 10'}
  ];
  
  app.get('/api', function(req, res){
    //req.i18n.setLng('es');
    
    res.json({
      title: req.i18n.t('title'),
      name: req.i18n.t('title'),
      PRODUCTS : PRODUCTS
    });
  });
  
};