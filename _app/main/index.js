'use strict';

var express = require('express');
var paths = require('../../_sys').cfg.paths;
var utils = require(paths.utils);

var app = express();
var options = {
  path: __dirname
};

options.routes = [
  { ctr: 'home.default', url: '/', view: 'default' },
  { ctr: 'home.test', url: '/test', view: 'test' },
  { ctr: 'home.about', url: '/about', view: 'about' },
  { ctr: 'home.inbox', url: '/inbox', view: 'inbox' },
  { ctr: 'home.testId', url: '/test/id', view: 'testId' }
];

utils.app.loadApp(app, options);

module.exports = app;
