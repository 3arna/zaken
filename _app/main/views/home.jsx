'use strict';

var React   = require('react'),
    lodash  = require('lodash'),
    $       = require('jquery'),
    views   = require('./views');
    
var Navigation  = views.components.navigation;
var Layout      = views.layout;
var ButtonSmall = views.components.buttonSmall;

var App = React.createClass({
  
  render: function(){
    var Child;
    
    var currentRoute = this.props.route;
    
    var route = lodash.find(this.props.routes, function(route){
      return route == currentRoute;
    });
    
    Child = route ? views[route] : views.default;

    return (
      <Layout {...this.props}>
        <ButtonSmall onClick={this.onClickShowProps}>display viewData on console</ButtonSmall>
        <Navigation routes={this.props.routes}/>
        <div>
          <h1>App</h1>
          <Child/>
        </div>
      </Layout>
    )
  },
  
  onClickShowProps: function(){
    console.log(this.props);
  },
  
  componentDidMount: function(){
    console.log('Home loaded on the client side');
  }
  
});

module.exports = App;

/*if(typeof window !== 'undefined'){
  
  window.onload = function(){
    
    function render(){
      viewData.route = window.location.pathname.replace('/', '');
      React.render(React.createFactory(App)(viewData), document);
    }
    
    $('a').on('click', function(){
      var route = $(this).attr('href').replace('#', '');
      window.history.pushState('', route, '/' + route);
      render();
      return false;
    });
    
    render();
  }
}*/