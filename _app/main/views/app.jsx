'use strict';

var React   = require('react'),
    lodash  = require('lodash'),
    $       = require('jquery'),
    Views   = require('./views');

var App = React.createClass({
  
  render: function(){
    var Page;
    
    var currentRoute = this.props.route;
    
    var route = lodash.find(this.props.routes, function(route){
      return route.url == currentRoute.url;
    });
    
    Page = route.view ? Views[route.view] : Views.default;

    return (
      <Views.layout {...this.props}>
        <Views.components.buttonSmall onClick={this.onClickShowProps}>display viewData on console</Views.components.buttonSmall>
        <Views.components.navigation routes={this.props.routes}/>
        <div>
          <h1><a href="/">Main App</a> / <a href="/sub">Sub App</a></h1>
          <Page/>
        </div>
      </Views.layout>
    )
  },
  
  onClickShowProps: function(){
    console.log(this.props);
  },
  
  componentDidMount: function(){
    console.log('Home loaded on the client side');
  }
  
});

module.exports = App;

typeof window !== 'undefined' &&  $(function() {
  
  window.onpopstate = function(e){
    viewData.route = lodash.find(viewData.routes, function(route){
      return window.location.pathname == route.url;
    });
    
    render();
  };
    
  function render(){
    React.render(React.createFactory(App)(viewData), document);
  }
  
  $('a').on('click', function(){
    var route = $(this).attr('href');
    window.history.pushState({route: route}, route, route);
    
    viewData.route = lodash.find(viewData.routes, function(route){
      return window.location.pathname == route.url;
    });
    
    render();
    return false;
  });
  
  render();
  
});