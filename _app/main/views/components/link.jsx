'use strict';

var React = require('react');

var Link = React.createClass({
  render: function(){
    return (
      <li><a href={this.props.route.url}>{this.props.route.url}</a></li>
    );
  }
});

module.exports = Link;