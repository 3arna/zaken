'use strict';

var express = require('express'),
    paths   = require(process.cwd() + '/_sys').cfg.paths,
    utils   = require(paths.utils);

var app = express();

utils.app.loadApp(app, {});

module.exports = app;
