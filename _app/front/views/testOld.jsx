'use strict';

var React = require('react');

var Link = React.createClass({
  render: function(){
    return (
      <li><a href={"#" + this.props.route}>{this.props.route}</a></li>
    );
  }
});

var About = React.createClass({
  render: function () {
    return <h2>About</h2>;
  }
});

var Inbox = React.createClass({
  render: function () {
    return <h2>Inbox</h2>;
  }
});

var Test = React.createClass({
  render: function () {
    return <h2>Test</h2>;
  }
});

var App = React.createClass({
  
  componentDidMount: function(){
    console.log('good morning vietnam');
  },
  
  render: function() {
    var Child;
    switch (this.props.route) {
      case 'about': Child = About; break;
      case 'inbox': Child = Inbox; break;
      default:      Child = Test;
    }

    return (
      <html>
        <head>
          <meta charSet='utf-8' />
          <title>sdasd</title>
        </head>
        <body>
          <ul>
            {this.props.routes.map(function(route){
              return <Link route={route}/>
            })}
          </ul>
          <div>
            <h1>App</h1>
            <Child/>
          </div>
        </body>
        <script src='/js/bundle.js'></script>
      </html>
    )
  }
});

module.exports = App;

if(typeof window !== 'undefined' && viewData.viewName == 'Test'){
  
  window.onload = function(){
    
    function render () {
      viewData.route = window.location.hash.substr(1);
      console.log(viewData);
      React.render(React.createFactory(App)(viewData), document);
    }
    
    
    window.addEventListener('hashchange', render);
    render();
    
  }
}