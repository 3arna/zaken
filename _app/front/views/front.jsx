'use strict';

var Layout = require('../../../_shared/views/layout.jsx');
var React = require('react');

var Front = module.exports = React.createClass({

  onButtonClick: function() {
    alert('I was rendered on server side but I am clickable because of client mounting!');
  },
  
  componentDidMount: function(){
    console.log('good morning vietnam');
  },

  render: function render() {
    return (
      <Layout {...this.props}>
        <div id='index'>
          <h1>Hello from front page {this.props.name}!</h1>
          <p>{this.props.text}</p>
          <button onClick={this.onButtonClick}>___Click Me___</button>
        </div>
      </Layout>
    );
  }
});

if(typeof window !== 'undefined' && viewData.viewName === 'Front'){
  (function(){
    React.render(React.createFactory(Front)(viewData), document);
  })();
};
