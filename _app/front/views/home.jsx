'use strict';

var Layout = require('../../../_shared/views/layout.jsx');
var ButtonSmall = require('./components/button-small.jsx');
var React = require('react');

var Home = React.createClass({
  
  onButtonClick: function(){
    alert('totaly new message from parent view');
  },

  render: function render(){
    return (
      <Layout {...this.props}>
        <div id='index'>
          <h1>Hello {this.props.name}!</h1>
          <ButtonSmall onButtonClick={this.onButtonClick}>Root main button</ButtonSmall>
        </div>
      </Layout>
    );
  }
});

module.exports = Home;

if(typeof window !== 'undefined' && viewData.viewName == 'Home'){
  window.onload = function(){
    React.render(React.createFactory(Home)(viewData), document);
  }
};