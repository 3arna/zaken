'use strict';

var React = require('react');
var Layout = require('../../../_shared/views/layout.jsx');
var Table = require('./components/table.jsx'); 

var Main = React.createClass({
  render: function() {
    return (
      <Layout {...this.props}>
        <Table products={this.props.PRODUCTS} />
      </Layout>
    );
  }
});

module.exports = Main;

if(typeof window !== 'undefined' && viewData.viewName === 'Main'){
  window.onload = function(){
    React.render(React.createFactory(Main)(viewData), document);
  }
};