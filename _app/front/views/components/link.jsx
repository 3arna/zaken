'use strict';

var React = require('react');

var Link = React.createClass({
  render: function(){
    return (
      <li><a href={"#" + this.props.route}>{this.props.route}</a></li>
    );
  }
});

module.exports = Link;