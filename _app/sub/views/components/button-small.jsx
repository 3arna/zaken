'use strict';

var React = require('react');

module.exports = React.createClass({
  
  render: function render() {
    return (
      <button className="button-small" onClick={this.props.onClick}>
        {this.props.children}
      </button>
    );
  }
});
