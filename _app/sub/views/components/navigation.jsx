'use strict';

var React = require('react');

var Link  = require('./link.jsx');

module.exports = React.createClass({
  render: function(){
    return (
      <ul>
        {this.props.routes.map(function(route, key){
          return <Link key={key} route={route}/>
        })}
      </ul>
    );
  }
});