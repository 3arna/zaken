exports.layout = require('../../../_shared/views/layout.jsx')

exports.test = require('./test.jsx');
exports.inbox = require('./inbox.jsx');
exports.about = require('./about.jsx');
exports.default = require('./default.jsx');
exports.testId = require('./test/id.jsx');

exports.components = {
  link:         require('./components/link.jsx'),
  navigation:   require('./components/navigation.jsx'),
  buttonSmall:  require('./components/button-small.jsx'),
}