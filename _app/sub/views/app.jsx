'use strict';

var React   = require('react'),
    lodash  = require('lodash'),
    $       = require('jquery'),
    views   = require('./views');
    
var Navigation  = views.components.navigation;
var Layout      = views.layout;
var ButtonSmall = views.components.buttonSmall;

var App = React.createClass({
  
  render: function(){
    var Page;
    
    var currentRoute = this.props.route;
    
    var route = lodash.find(this.props.routes, function(route){
      return route.url == currentRoute.url;
    });
    
    Page = route.view ? views[route.view] : views.default;

    return (
      <Layout {...this.props}>
        <ButtonSmall onClick={this.onClickShowProps}>display viewData on console</ButtonSmall>
        <Navigation routes={this.props.routes}/>
        <div>
          <h1><a href="/">App</a></h1>
          <Page/>
        </div>
      </Layout>
    )
  },
  
  onClickShowProps: function(){
    console.log(this.props);
  },
  
  componentDidMount: function(){
    console.log('Home loaded on the client side');
  }
  
});

module.exports = App;

if(typeof window !== 'undefined'){
  
  window.onload = function(){
    
    function render(){
      React.render(React.createFactory(App)(viewData), document);
    }
    
    $('a').on('click', function(){
      var route = $(this).attr('href');
      window.history.pushState('', route, route);
      
      viewData.route = lodash.find(viewData.routes, function(route){
        return window.location.pathname == route.url;
      });
      
      render();
      return false;
    });
    
    render();
    
  }
}