'use strict';

module.exports = function(app){
  
  var PRODUCTS = [
    {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
    {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
    {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
    {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
    {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
    {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'},
    {category: 'Electronics', price: '$97.99', stocked: false, name: 'Nexus 9'},
    {category: 'Electronics', price: '$52.99', stocked: false, name: 'Nexus 10'}
  ];
  
  var routes = [
    {
      url : '/sub',
      view : 'default'
    },
    {
      url : '/sub/test',
      view : 'test'
    },
    {
      url : '/sub/about',
      view : 'about'
    },
    {
      url : '/sub/inbox',
      view : 'inbox'
    },
    {
      url : '/sub/test/id',
      view : 'testId'
    }
  ];
  
  app.get('/sub/', function(req, res){
    //req.i18n.setLng('es');
    var viewData = {
      route: req.url,
      routes: routes
    };
    
    viewData.title = req.i18n.t('title');
    res.render('default', viewData);
  });
  
  app.get('/sub/test', function(req, res){
    var viewData = {
      routes: routes,
      route: req.url,
    }
    res.render('test', viewData);
  });
  
  app.get('/sub/test/id', function(req, res){
    var viewData = {
      routes: routes,
      route: req.url,
    }
    res.render('test/id', viewData);
  });
  
  app.get('/sub/about', function(req, res){
    var viewData = {
      routes: routes,
      route: req.url,
    }
    res.render('about', viewData);
  });
  
  app.get('/sub/inbox', function(req, res){
    var viewData = {
      routes: routes,
      route: req.url,
    }
    res.render('inbox', viewData);
  });
  
};