(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  render: function () {
    return React.createElement("h2", null, "About Sub");
  }
});

},{"react":undefined}],2:[function(require,module,exports){
'use strict';

var React   = require('react'),
    lodash  = require('lodash'),
    $       = require('jquery'),
    views   = require('./views');
    
var Navigation  = views.components.navigation;
var Layout      = views.layout;
var ButtonSmall = views.components.buttonSmall;

var App = React.createClass({displayName: "App",
  
  render: function(){
    var Page;
    
    var currentRoute = this.props.route;
    
    var route = lodash.find(this.props.routes, function(route){
      return route.url == currentRoute.url;
    });
    
    Page = route.view ? views[route.view] : views.default;

    return (
      React.createElement(Layout, React.__spread({},  this.props), 
        React.createElement(ButtonSmall, {onClick: this.onClickShowProps}, "display viewData on console"), 
        React.createElement(Navigation, {routes: this.props.routes}), 
        React.createElement("div", null, 
          React.createElement("h1", null, React.createElement("a", {href: "/"}, "App")), 
          React.createElement(Page, null)
        )
      )
    )
  },
  
  onClickShowProps: function(){
    console.log(this.props);
  },
  
  componentDidMount: function(){
    console.log('Home loaded on the client side');
  }
  
});

module.exports = App;

if(typeof window !== 'undefined'){
  
  window.onload = function(){
    
    function render(){
      React.render(React.createFactory(App)(viewData), document);
    }
    
    $('a').on('click', function(){
      var route = $(this).attr('href');
      window.history.pushState('', route, route);
      
      viewData.route = lodash.find(viewData.routes, function(route){
        return window.location.pathname == route.url;
      });
      
      render();
      return false;
    });
    
    render();
    
  }
}

},{"./views":10,"jquery":undefined,"lodash":undefined,"react":undefined}],3:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  
  render: function render() {
    return (
      React.createElement("button", {className: "button-small", onClick: this.props.onClick}, 
        this.props.children
      )
    );
  }
});

},{"react":undefined}],4:[function(require,module,exports){
'use strict';

var React = require('react');

var Link = React.createClass({displayName: "Link",
  render: function(){
    return (
      React.createElement("li", null, React.createElement("a", {href: this.props.route.url}, this.props.route.url))
    );
  }
});

module.exports = Link;

},{"react":undefined}],5:[function(require,module,exports){
'use strict';

var React = require('react');

var Link  = require('./link.jsx');

module.exports = React.createClass({displayName: "exports",
  render: function(){
    return (
      React.createElement("ul", null, 
        this.props.routes.map(function(route, key){
          return React.createElement(Link, {key: key, route: route})
        })
      )
    );
  }
});

},{"./link.jsx":4,"react":undefined}],6:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  render: function(){
    return React.createElement("h2", null, "Sub home");
  }
});

},{"react":undefined}],7:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  render: function(){
    return React.createElement("h2", null, "Sub Inbox");
  }
});

},{"react":undefined}],8:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  render: function () {
    return React.createElement("h2", null, "Sub Test");
  }
});

},{"react":undefined}],9:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  render: function () {
    return React.createElement("h2", null, "Sub Test Id");
  }
});

},{"react":undefined}],10:[function(require,module,exports){
exports.layout = require('../../../_shared/views/layout.jsx')

exports.test = require('./test.jsx');
exports.inbox = require('./inbox.jsx');
exports.about = require('./about.jsx');
exports.default = require('./default.jsx');
exports.testId = require('./test/id.jsx');

exports.components = {
  link:         require('./components/link.jsx'),
  navigation:   require('./components/navigation.jsx'),
  buttonSmall:  require('./components/button-small.jsx'),
}

},{"../../../_shared/views/layout.jsx":11,"./about.jsx":1,"./components/button-small.jsx":3,"./components/link.jsx":4,"./components/navigation.jsx":5,"./default.jsx":6,"./inbox.jsx":7,"./test.jsx":8,"./test/id.jsx":9}],11:[function(require,module,exports){
'use strict';

var React = require('react');

module.exports = React.createClass({displayName: "exports",
  
  getDefaultProps: function(){
    return {
      title: 'default title',
      childrend: '',
      appName: 'main'
    };
  },
  
  render: function(){
    return (
      React.createElement("html", null, 
        React.createElement("head", null, 
          React.createElement("meta", {charSet: "utf-8"}), 
          React.createElement("title", null, 
            this.props.title
          ), 
          React.createElement("link", {rel: "stylesheet", type: "text/css", href: '/css/' + this.props.settings.appName + '.css'})
        ), 
        React.createElement("body", null, 
          this.props.children
        ), 
        React.createElement("script", {src: "/js/deps.js"}), 
        React.createElement("script", {src: '/js/' + this.props.settings.appName + '.js'})
      )
    );
  }
});

},{"react":undefined}]},{},[2]);
